package com.example.connect3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    int player = 0;
    boolean winner = false;
    Vibrator v ;

    TextView winnerTextView;
    Button newGameBtn ;
    int gameCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        winnerTextView = (TextView) findViewById(R.id.winnerTextView);
        newGameBtn = findViewById(R.id.button2);
        v =(Vibrator) getSystemService(Context.VIBRATOR_SERVICE);;
    }

    public int[] gamePositions = {2, 2, 2, 2, 2, 2, 2, 2, 2};
    int[][] winningPositions = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}, {0, 3, 6}, {1, 4, 7}, {2, 5, 8}, {0, 4, 8}, {2, 4, 6}};

    public void playerMove(View view) {
        ImageView imageView = (ImageView) view;
        int positionInstance = gamePositions[Integer.parseInt(imageView.getTag().toString())];
        int position = Integer.parseInt(imageView.getTag().toString());

        Log.i("positin", "" + position);
        Log.i("positin", "" + imageView.getTag());
        Log.i("positin", "" + Arrays.toString(gamePositions));
        if (positionInstance == 2 && !winner) {

            if (player == 0) {
                imageView.setImageResource(R.drawable.red);
                gamePositions[position] = player;
                imageView.setTranslationY(-1000f);
                imageView.animate().translationY(0f).rotation(360).setDuration(300);
            } else {
                imageView.setImageResource(R.drawable.yellow);
                gamePositions[position] = player;
                imageView.setTranslationY(-1000f);
                imageView.animate().translationY(0f).rotation(360).setDuration(300);
            }
            for (int[] winningPosition : winningPositions) {
                if (gamePositions[winningPosition[0]] == player && gamePositions[winningPosition[1]] == player && gamePositions[winningPosition[2]] == player) {
                    winner = true;
                    newGameBtn.setEnabled(true);
                    if (player == 0) {
                        winnerTextView.setTextColor(Color.rgb(200, 0, 0));
                        winnerTextView.setText("Player RED Won");
                        v.vibrate(1000);
                    } else {
                        winnerTextView.setTextColor(Color.rgb(255, 255, 0));
                        winnerTextView.setText("Player YELLOW Won");
                        v.vibrate(1000);


                    }
                    break;
                }
            }
            if (!winner) {
                switchPlayer();
                gameCounter++;
                if (gameCounter ==9){
                    winnerTextView.setTextColor(Color.rgb(255,255,255));
                    winnerTextView.setText("Game ended Draw");
                    newGameBtn.setEnabled(true);
                    v.vibrate(1000);
                }

            }

        }
    }

    public void switchPlayer() {
        if (player == 0) {
            player = 1;
        } else {
            player = 0;
        }
    }

    public void newGame(View view) {
        Arrays.fill(gamePositions, 2);
        winnerTextView.setText(null);
        androidx.gridlayout.widget.GridLayout gridLayout = findViewById(R.id.gameLayout);

        player = 0;
        winner=false;
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            ((ImageView) gridLayout.getChildAt(i)).setImageResource(0);
        }
        newGameBtn.setEnabled(false);
        gameCounter=0;

    }
    public void exitGame(View view){
        finish();
        System.exit(0);
    }
}
